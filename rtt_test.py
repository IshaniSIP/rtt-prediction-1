#!/usr/bin/python

# Arjun Subramonian
# A Machine Learning Approach to End-to-End RTT Estimation and its Application to TCP
# TCP Retransmission Timer Adjustment Mechanism Using Model-Based RTT Predictor
# Jitter-Based Delay-Boundary Prediction of Wide-Area Networks
# Jacobson

from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson

from fileinput import input
import datetime

if __name__ == "__main__":
    predictors = [
        MachineLearning(),
        #ModelBased(),
        JitterPredictor(input()),
        Jacobson(),
    ]

    count = 0
    prevTime = None
    td = None
    for line in input():
        arr = line.strip().split()
        rtt = float(arr[0])
        time = datetime.datetime.strptime(arr[-2] + " " + arr[-1], "%Y-%m-%d %H:%M:%S.%f")
        if prevTime is not None:
            td = (time - prevTime).total_seconds()
        for p in predictors:
            if type(p) is MachineLearning:
                p.receive_rtt(rtt, -1, count)
            elif type(p) is ModelBased:
                p.receive_rtt(rtt, time, count)
            elif type(p) is JitterPredictor and count != 0:
                p.receive_rtt(rtt, td)
            elif type(p) is Jacobson:
                p.receive_rtt(rtt)
        prevTime = time
        count += 1
    if count != 0:
        for p in predictors:
            print type(p).__name__, p.lost_packets*100.0/count,
            print p.error
    
