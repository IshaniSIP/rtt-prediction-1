import pyshark
from sys import argv
import netaddr

target = 2154969872
d = {}
for filename in argv[1:]:
    pcap = pyshark.FileCapture(filename)
    for pkt in pcap:
        src = int(netaddr.IPAddress(pkt.ip.src))
        dst = int(netaddr.IPAddress(pkt.ip.dst))
        try:
            ack = pkt.tcp.ack
        except(AttributeError):
            ack = 0
        seq = pkt.tcp.seq
        len = pkt.tcp.len
        dt  = pkt.sniff_time
        if dst == target or src == target:
            print src, dst, ack, seq, int(seq)+int(len), dt
        #pair = "%d-%d" % (src, dst)
        #if src > dst:
            #pair = "%d-%d" % (dst, src)
        #if pair not in d:
            #d[pair] = 0
        #d[pair] += 1

#print "Pairs:"
#for pair in d.keys():
    #print pair, d[pair]
