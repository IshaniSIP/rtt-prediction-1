from fileinput import input
import datetime

class LostPacket(Exception):
    def __init__(self, ack):
        self.ack = ack

def get_time(ack, p):
    k = 0
    for (i, f, d) in p:
        if ack <= f and ack >= i:
            t = d
            break
        k += 1
    if k == len(p):
        raise LostPacket(ack)
    del p[k]
    return t

ack_counter = 0
sent = []
sender = 0
for line in input():
    values = line.strip().split(" ", 5)
    src, dest, ack, iseq, fseq = [int(x) for x in values[:-1]]
    date = datetime.datetime.strptime(values[-1], "%Y-%m-%d %H:%M:%S.%f")

    if ack == 0 and iseq == 0:
        sender = src
        sent = [(0, 1, date)]
        ack_counter = seq_counter = 0
        continue
    if src == sender:
        sent.append((iseq, fseq, date))
    elif dest == sender:
        #import pdb; pdb.set_trace()
        try:
            d = get_time(ack, sent)
            print (date-d).total_seconds()*1000
        except LostPacket:
            pass
            #print "lost"
