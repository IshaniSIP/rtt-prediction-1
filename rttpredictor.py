import datetime

class RTTPredictor(object):
    def __init__(self, under_error = 1, over_error = -1):
        """Initiates the algorithm's parameters with default values"""
        self.under_error = under_error
        self.over_error = over_error
        self.rto = 1000
        self.error = 0
        self.lost_packets = 0
        # timer granularity in milliseconds
        self.granularity = 100
        
    def receive_rtt(self, rtt, t = -1, count = -1):
        """Receive the next measured RTT and update internal parameters."""
        d = self.rto - rtt
        if d < 0:
            self.error += self.over_error * d
            if t == -1 and count == -1:
                self.process_loss()
            elif t == -1 and count != -1:
                self.process_loss(rtt, count)
            elif type(t) is datetime.timedelta and count == -1:
                self.process_loss(rtt, t)
            elif type(t) is datetime.datetime and count != -1:
                self.process_loss(rtt, t, count)
        else:
            self.error += self.under_error * d
            if t == -1 and count == -1:
                self.update_rto(rtt)
            elif t == -1 and count != -1:
                self.update_rto(rtt, count)
            elif type(t) is datetime.timedelta and count == -1:
                self.update_rto(rtt, t)
            elif type(t) is datetime.datetime and count != -1:
                self.update_rto(rtt, t, count)

    def update_rto(self):
        self.rto = rtt

    def process_loss(self):
        self.lost_packets += 1

