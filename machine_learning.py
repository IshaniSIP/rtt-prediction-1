from rttpredictor import RTTPredictor
import numpy as np
import math

class MachineLearning(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, N = 7.0, learning_rate = 1.0, sharing_rate = 0.5):
        RTTPredictor.__init__(self, under_error, over_error)
        self.N = N
        self.learning_rate = learning_rate
        self.sharing_rate = sharing_rate
        self.RTT_min = 1
        self.RTT_max = 128
        self.experts = self.fill_experts()
        self.y = []
        self.weights = {}
        self.update_weights(1000, -1)

    def fill_experts(self):
        n = int(self.N)
        experts = []
        for i in range(1, n + 1):
            experts.append(self.RTT_min + self.RTT_max * (2 ** ((i - self.N) / 4)))
        return experts

    def compute_loss(self, expert, rtt):
        if expert >= rtt:
            return (expert - rtt) ** 2
        else:
            return 2 * rtt

    def calc_pool(self, wprime):
        return self.sharing_rate * math.fsum(wprime)

    def update_weights(self, rtt, count):
        n = int(self.N)
        if count == -1:
            self.weights[0] = [1 / self.N] * n
        else:
            self.weights[count + 1] = []
            wprime = []
            for i in range(0, n):
                wprime.append(self.weights[count][i] * math.expm1(-self.learning_rate * self.compute_loss(self.experts[i], rtt)))
            pool = self.calc_pool(wprime)
            for i in range(0, n):
                self.weights[count + 1].append((1 - self.sharing_rate) * wprime[i] + 1 / self.N * pool)

    def new_rto(self, count):
        num_sum = 0
        denom_sum = 0
        for i in range(0, int(self.N)):
            num_sum += self.weights[count][i] * self.experts[i]
            denom_sum += self.weights[count][i]
        return num_sum / denom_sum

    def update_rto(self, rtt, count):
        self.rto = self.new_rto(count)
        self.update_weights(rtt, count)

    def process_loss(self, rtt, count):
        RTTPredictor.process_loss(self)
        self.rto = self.new_rto(count)
        self.update_weights(rtt, count)


