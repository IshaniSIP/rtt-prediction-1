from rttpredictor import RTTPredictor
import datetime
import numpy as np
from numpy.linalg import pinv

class ModelBased(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, lamb = 0.97):
        RTTPredictor.__init__(self, under_error, over_error)
        self.lamb = lamb
        self.hist = []

    def u(self, k):
        if k <= 0:
            return 0
        return (self.hist[k][1] - self.hist[k - 1][1]).total_seconds()

    def deltay(self, k):
        if k <= 0:
            return 0
        return self.hist[k][0] - self.hist[k - 1][0]

    def deltau(self, k):
        return self.u(k) - self.u(k - 1)

    def phi(self, k):
        phi1 = []
        timer = 1
        while timer <= k:
            phi1.append(-self.deltay(k - timer))
            timer += 1
        phi2 = []
        timer = 1
        while timer <= k:
            phi2.append(self.deltau(k - timer))
            timer += 1
        phis = np.vstack((np.asarray(phi1), np.asarray(phi2)))
        mat = np.matrix(phis)
        return (mat, mat.T)

    def P(self, k):
        phis = self.phi(k)
        if k <= 1:
            return (1 / self.lamb) * (1 - phis[0].dot(phis[1]) / (self.lamb + phis[0].dot(phis[1])))
        else:
            pminus = self.P(k - 1)
            #if pminus.size == 1:
            pminus = pminus.item(0)
            return (1 / self.lamb) * (pminus - ((pminus * phis[0]).dot(phis[1]) * pminus) / (self.lamb + (phis[0] * pminus).dot(phis[1])))
            #else:
                #return (1 / self.lamb) * (pminus - pminus.dot(phis[0]).dot(phis[1]).dot(pminus) / (self.lamb + pminus.dot(phis[0]).dot(phis[1])))

    def L(self, k):
        pminus = self.P(k - 1)
        phis = self.phi(k)
        #if pminus.size == 1:
        pminus = pminus.item(0)
        return (phis[1] * pminus).dot(pinv(self.lamb + (pminus * phis[0]).dot(phis[1])))
        #else:
            #return phis[1].dot(pminus).dot(pinv(self.lamb + pminus.dot(phis[0]).dot(phis[1])))

    def theta(self, k):
        if k <= 1:
            return 1 + self.L(k).dot(self.deltay(k) - self.phi(k)[0])
        else:
            tminus = self.theta(k - 1)
            #if tminus.size == 1:
            tminus = tminus.item(0)
            return tminus + (self.deltay(k) - tminus * self.phi(k)[0]).dot(self.L(k))
            #else:
                #return tminus + (self.deltay(k) - tminus.dot(self.phi(k)[0])).dot(self.L(k))

    def deltayhat(self, k):
        coeffs = np.ravel(self.theta(k).T)
        acoeffs = coeffs[0:coeffs.size / 2]
        bcoeffs = coeffs[coeffs.size / 2:]
        res = 0
        sub = 0
        while sub <= acoeffs.size - 1:
            res += -acoeffs[sub] * self.deltay(k - sub)
            sub += 1
        sub = 0
        while sub <= bcoeffs.size - 1:
            res += bcoeffs[sub] * self.deltau(k - sub)
            sub += 1
        return res

    def yhat(self, k):
        return self.hist[k][0] + self.deltayhat(k)

    def update_rto(self, rtt, t, k):
        self.hist.append((rtt, t))
        if k != 0 and k != 1:
            self.rto = self.yhat(k)

    def process_loss(self, rtt, t, k):
        RTTPredictor.process_loss(self)
        self.hist.append((rtt, t))
        if k != 0 and k != 1:
            self.rto = self.yhat(k)

