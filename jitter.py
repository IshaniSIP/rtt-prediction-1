from rttpredictor import RTTPredictor
import datetime
import numpy as np

class JitterPredictor(RTTPredictor):
	def __init__(self, f, under_error = 1, over_error = -1, k = 2.6):
		RTTPredictor.__init__(self, under_error, over_error)
		count = 0
		self.hist = {}
		prevRTT = None
		prevTime = None
		for line in f:
		    arr = line.strip().split()
		    rtt = float(arr[0])
		    time = datetime.datetime.strptime(arr[-2] + " " + arr[-1], "%Y-%m-%d %H:%M:%S.%f")
		    if count == 0:
		        prevRTT = rtt
		        prevTime = time
		        count += 1
		        continue
		    td = (time - prevTime).total_seconds()
		    if td not in self.hist:
		        self.hist[td] = []
		    self.hist[td].append(rtt - prevRTT)
		    prevRTT = rtt
		    prevTime = time
		    count += 1
		for key in self.hist:
		    arr = np.asarray(self.hist[key])
		    self.hist[key] = np.std(arr)
		self.k = k

	def update_rto(self, rtt, td):
		self.rto = rtt - self.k * self.hist[td]

	def process_loss(self, rtt, td):
		RTTPredictor.process_loss(self)
		self.rto = rtt + self.k * self.hist[td]

